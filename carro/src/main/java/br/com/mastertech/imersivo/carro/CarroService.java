package br.com.mastertech.imersivo.carro;

import java.util.UUID;

import org.springframework.stereotype.Service;

@Service
public class CarroService {

	public Carro novoCarro(String modelo) {
		Carro carro = new Carro();
		carro.setModelo(modelo);
		carro.setPlaca(UUID.randomUUID().toString());
		return carro;
	}
	
}
