package br.com.mastertech.imersivo.pessoa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {
	
	@Autowired
	private CarroClient carroClient;

	public Pessoa criaPessoa(String nome, String modelo) {
		Pessoa pessoa = new Pessoa();
		pessoa.setNome(nome);
		pessoa.setCarro(carroClient.criaCarro(modelo));
		return pessoa;
	}
	
}
